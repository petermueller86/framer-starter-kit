#!/bin/bash

# Starts a Python server without any dependencies.

echo ""
echo "`tput bold`View prototype:`tput sgr0`    Open a browser and go to `tput setaf 2;tput bold`http://localhost:8000/`tput sgr0`"
echo ""
echo "`tput bold`Shut down server:`tput sgr0`  Press `tput setaf 1;tput bold`ctrl-c`tput sgr0`. If you forget to shut it down you"
echo "                   can close it via the Activity Monitor app. Search"
echo "                   for the 'Python' process and hit 'quit'."
echo ""

# Jump to the folder of the script
cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd

# Open http://localhost:8000/
open http://localhost:8000/

# Mac only: Starts a simple Python server
python -m SimpleHTTPServer 8000
