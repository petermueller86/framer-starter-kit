# Welcome to Framer

# This is just demo code. Feel free to delete it all.

device = new Framer.DeviceView()
device.setupContext()
device.deviceType = "iphone-6-silver-hand"
device.contentScale = 1

imageLayer = new Layer
	width: 256
	height: 256
	image: "images/Icon.png"

imageLayer.center()

# Define a set of states with names (the original state is 'default')
imageLayer.states.add
	second:
		y: 300
		scale: 0.6
		rotationZ: 100
	third:
		y: 700
		scale: 1.3
	fourth:
		y: 600
		scale: 0.9
		rotationZ: 200

# Set the default animation options
imageLayer.states.animationOptions =
	curve: "spring(500,12,0)"

# On a click, go to the next state
imageLayer.onClick (event, layer) ->
	imageLayer.states.next()
