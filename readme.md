# Framer.js Starter Kit 🚀

This is a simple framer.js starter kit that runs right out-of-the-box and doesn't require the [Framer App](http://framerjs.com).

It is designed as a template for building a prototype. The Starter Kit uses CoffeeScript and a simple web-server for previewing the prototype like the original Framer app does. The goal behind this kit is to keep it as simple as possible: No dependencies or installations are required.


## Get Started

1. [Download](https://github.com/petermueller86/framer-starter-kit/archive/master.zip) the Framer.js Starter Kit.

2. Open the file **start_local_server.sh** in the terminal. It will start a server on your machine and open the prototype in a browser window.

  ![Shell script](https://cdn.rawgit.com/petermueller86/files/e846e4802ec6e7252538121fce2b13c647ab9d15/02_framer_starter_kit/screen_start_local_server.png)

  ![Browser preview](https://cdn.rawgit.com/petermueller86/files/0cf4f060719087d15704334b043e31c92efafb21/02_framer_starter_kit/screen_browser.png)

3. Now you should see an iPhone with a little icon that jumps around when you click it. That's just some demo content. To create your own prototype, open and edit the file **app.coffee**. Write all your CoffeeScript code in here and don't forget to refresh the browser to see the changes.

  ![App.coffee](https://cdn.rawgit.com/petermueller86/files/224ffa5d0effc5ab851f98c655478d5e64e09e5d/02_framer_starter_kit/screen_app_coffee.png)

4. Be amazing! ✨🚀

5. To shut down the server hit `ctrl-c` in the terminal window.


## Resources

* [Framer getting started](http://framerjs.com/learn/basics/)  
  Basics about layers, animations, events and the importer. Also explains CoffeeScript a bit.

* [Framer documentation](http://framerjs.com/docs/)  
  Documentation of the Framer.js library.

* [Tutorial collection](http://framerjs.com/resources/)  
  A great collection of eBooks, video courses and tutorials about framer.js.

* [Framer Slack group](https://framer-slack-signup.herokuapp.com/)  
  Slack server full of Frameristas from around the world. Get help or just some inspiration. Great channels are #realtimehelp and #general.
